<?php

namespace IPC\SecurityBundle\Form\Model;

class ChangePassword
{
    /**
     * Current password
     *
     * @var string
     */
    protected $current;

    /**
     * New password
     *
     * @var string
     */
    protected $new;

    /**
     * Repeated password
     *
     * @var string
     */
    protected $repeated;

    /**
     * @return string
     */
    public function getCurrent(): ?string
    {
        return $this->current;
    }

    /**
     * @param string $current
     *
     * @return $this
     */
    public function setCurrent(?string $current): self
    {
        $this->current = $current;

        return $this;
    }

    /**
     * @return string
     */
    public function getNew(): ?string
    {
        return $this->new;
    }

    /**
     * @param string $new
     *
     * @return $this
     */
    public function setNew(?string $new): self
    {
        $this->new = $new;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getRepeated(): ?string
    {
        return $this->repeated;
    }

    /**
     * @param string $repeated
     *
     * @return $this
     */
    public function setRepeated(?string $repeated): self
    {
        $this->repeated = $repeated;

        return $this;
    }
}
