<?php

namespace IPC\Tests\SecurityBundle\Form\Type;

use IPC\SecurityBundle\Form\Model\ChangePassword;
use IPC\SecurityBundle\Form\Type\ChangePasswordType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Validator\Type\FormTypeValidatorExtension;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Mapping\ClassMetadata;

/**
 * @coversDefaultClass \IPC\SecurityBundle\Form\Type\ChangePasswordType
 */
class ChangePasswordTypeTest extends TypeTestCase
{
    /**
     * @var OptionsResolver
     */
    protected $resolver;

    /**
     * @var ChangePasswordType
     */
    protected $form;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->resolver = new OptionsResolver();
        $this->form     = new ChangePasswordType();

        $this->validator = $this->getMockBuilder(ValidatorInterface::class)->getMock();
        $metadata = $this->getMockBuilder(ClassMetadata::class)->disableOriginalConstructor()->getMock();
        $this->validator->expects($this->any())->method('getMetadataFor')->will($this->returnValue($metadata));
        $this->validator->expects($this->any())->method('validate')->will($this->returnValue(array()));

        parent::setUp();
    }

    /**
     * @return array
     */
    protected function getExtensions(): array
    {
        return array_merge(parent::getExtensions(), array(
            new ValidatorExtension($this->validator),
        ));
    }

    /**
     * @return array
     */
    protected function getTypeExtensions(): array
    {
        return [
            new FormTypeValidatorExtension($this->validator),
        ];
    }

    /**
     * @return void
     *
     * @covers ::configureOptions()
     */
    public function testConfigureOptions(): void
    {
        $this->form->configureOptions($this->resolver);

        $this->assertTrue($this->resolver->hasDefault('data_class'));
        $this->assertTrue($this->resolver->hasDefault('validation_groups'));
        $this->assertTrue($this->resolver->isRequired('require_current'));
        $this->assertTrue($this->resolver->isRequired('require_repeated'));

        $options = $this->resolver->resolve(['require_current' => false, 'require_repeated' => false]);

        $this->assertEquals(ChangePassword::class, $options['data_class']);
        $this->assertInternalType('callable', $options['validation_groups']);
        $this->assertEquals([Constraint::DEFAULT_GROUP], $options['validation_groups']());
        $this->assertEquals(false, $options['require_current']);
        $this->assertEquals(false, $options['require_repeated']);
    }

    /**
     * @return void
     *
     * @covers ::buildForm()
     */
    public function testBuildFormNoCurrentNoRepeated(): void
    {
        $this->form->configureOptions($this->resolver);

        $options = $this->resolver->resolve(['require_current' => false, 'require_repeated' => false]);

        $this->assertCount(0, $this->builder);

        $this->form->buildForm($this->builder, $options);

        $this->builder->all(); // fix for FormBuilder vs SubmitFormBuilder issue

        $this->assertCount(2, $this->builder);

        $this->assertNew();
        $this->assertUpdate();

        $this->assertValidationGroups([Constraint::DEFAULT_GROUP]);
    }

    /**
     * @return void
     *
     * @covers ::buildForm()
     */
    public function testBuildFormNoRepeated(): void
    {
        $this->form->configureOptions($this->resolver);

        $options = $this->resolver->resolve(['require_current' => true, 'require_repeated' => false]);

        $this->assertCount(0, $this->builder);

        $this->form->buildForm($this->builder, $options);

        $this->builder->all(); // fix for FormBuilder vs SubmitFormBuilder issue

        $this->assertCount(3, $this->builder);

        $this->assertCurrent();
        $this->assertNew();
        $this->assertUpdate();

        $this->assertValidationGroups([Constraint::DEFAULT_GROUP, 'require_current']);
    }

    /**
     * @return void
     *
     * @covers ::buildForm()
     */
    public function testBuildFormCurrentRepeated(): void
    {
        $this->form->configureOptions($this->resolver);

        $options = $this->resolver->resolve(['require_current' => true, 'require_repeated' => true]);

        $this->assertCount(0, $this->builder);

        $this->form->buildForm($this->builder, $options);

        $this->builder->all(); // fix for FormBuilder vs SubmitFormBuilder issue

        $this->assertCount(3, $this->builder);

        $this->assertCurrent();
        $this->assertRepeated();
        $this->assertUpdate();

        $this->assertValidationGroups([Constraint::DEFAULT_GROUP, 'require_current', 'require_repeated']);
    }

    /**
     * @return void
     */
    protected function assertCurrent(): void
    {
        $this->assertTrue($this->builder->has('current'));
        $current = $this->builder->get('current');
        $this->assertFalse($current->getOption('required'));
        $this->assertEquals('form.type.change_password.current.label', $current->getOption('label'));
    }

    /**
     * @return void
     */
    protected function assertRepeated(): void
    {
        $this->assertTrue($this->builder->has('new'));
        $repeated = $this->builder->get('new');
        $this->assertEquals(RepeatedType::class, \get_class($repeated->getType()->getInnerType()));
        $this->assertFalse($repeated->getOption('required'));
        $this->assertEquals('new', $repeated->getOption('first_name'));
        $this->assertEquals('repeated', $repeated->getOption('second_name'));
        $this->assertEquals(['label' => 'form.type.change_password.new.label'], $repeated->getOption('first_options'));
        $this->assertEquals(['label' => 'form.type.change_password.repeated.label'], $repeated->getOption('second_options'));

        $this->assertEquals('form.validation.change_password.new.invalid_message', $repeated->getOption('invalid_message'));
    }

    /**
     * @return void
     */
    protected function assertNew(): void
    {
        $this->assertTrue($this->builder->has('new'));
        $new = $this->builder->get('new');
        $this->assertEquals(PasswordType::class, \get_class($new->getType()->getInnerType()));
        $this->assertFalse($new->getOption('required'));
        $this->assertEquals('form.type.change_password.new.label', $new->getOption('label'));
    }

    /**
     * @return void
     */
    protected function assertUpdate(): void
    {
        $this->assertTrue($this->builder->has('update'));
        $update = $this->builder->get('update');
        $this->assertEquals('form.type.change_password.update.label', $update->getOption('label'));
    }

    /**
     * @param array $validationGroups
     *
     * @return void
     */
    protected function assertValidationGroups(array $validationGroups): void
    {
        $reflectionProperty = new \ReflectionProperty(ChangePasswordType::class, 'validationGroups');
        $reflectionProperty->setAccessible(true);
        $this->assertEquals($validationGroups, $reflectionProperty->getValue($this->form));
    }
}