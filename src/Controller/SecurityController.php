<?php

namespace IPC\SecurityBundle\Controller;

use Doctrine\Common\Persistence\ManagerRegistry;
use IPC\SecurityBundle\Entity\AdvancedUserTrait;
use IPC\SecurityBundle\Form\Model\ChangePassword;
use IPC\SecurityBundle\User\DoctrineUserProvider;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\Translation\TranslatorInterface;

class SecurityController extends Controller
{
    use TargetPathTrait;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param AuthenticationUtils $authUtils
     *
     * @return Response
     */
    public function login(AuthenticationUtils $authUtils): Response
    {
        $error        = $authUtils->getLastAuthenticationError();
        $lastUsername = $authUtils->getLastUsername();

        $formClass       = $this->getParameter('ipc_security.login.form');
        $viewTemplate    = $this->getParameter('ipc_security.login.view');
        $flashBagOptions = $this->getParameter('ipc_security.login.flash_bag');

        $redirectCredentialsExpired = $this->getParameter('ipc_security.credentials_expired.enabled');
        $routeCredentialsExpired    = $this->getParameter('ipc_security.credentials_expired.route');

        if ($redirectCredentialsExpired && $error instanceof CredentialsExpiredException) {
            return $this->redirectToRoute($routeCredentialsExpired);
        }

        if ($error && $flashBagOptions && $flashBagOptions['type']['error']) {
            $type = $flashBagOptions['type']['error'];
            $this->addFlash($type, $this->translator->trans($error->getMessage()));
        }

        $loginForm = $this->createForm($formClass);

        return $this->render($viewTemplate, [
            'last_username' => $lastUsername,
            'error'         => $error,
            'login'         => $loginForm->createView(),
        ]);
    }

    /**
     * Change the user password
     *
     * @param Request                 $request
     * @param AuthenticationUtils     $authUtils
     * @param TokenStorageInterface   $tokenStorage
     * @param DoctrineUserProvider    $userProvider
     * @param EncoderFactoryInterface $encoderFactory
     * @param ManagerRegistry         $managerRegistry
     *
     * @return Response
     *
     * @throws \RuntimeException     When no password encoder could be found for the user
     * @throws \LogicException       If DoctrineBundle is not available
     * @throws AccessDeniedException
     */
    public function credentialsExpired(
        Request $request,
        AuthenticationUtils $authUtils,
        TokenStorageInterface $tokenStorage,
        DoctrineUserProvider $userProvider,
        EncoderFactoryInterface $encoderFactory,
        ManagerRegistry $managerRegistry
    ): Response {
        try {
            if (!$this->getParameter('ipc_security.credentials_expired.enabled')) {
                throw new \RuntimeException(
                    $this->translator->trans('ipc_security.credentials_expired.not_enabled')
                );
            }

            $user = $userProvider->loadUserByUsername($authUtils->getLastUsername());

            if (!method_exists($user, 'isCredentialsExpired')
                || !$user->isCredentialsExpired())
            {
                throw new \RuntimeException(
                    $this->translator->trans('ipc_security.credentials_expired.user_not_supported')
                );
            }
        } catch (\Exception $e) {
            throw $this->createAccessDeniedException(
                $this->translator->trans('ipc_security.credentials_expired.access_denied'),
                $e
            );
        }

        $tokenStorage->setToken(new UsernamePasswordToken($user, '', '_'));

        $formClass       = $this->getParameter('ipc_security.credentials_expired.form');
        $options         = $this->getParameter('ipc_security.credentials_expired.form_options');
        $viewTemplate    = $this->getParameter('ipc_security.credentials_expired.view');
        $flashBagOptions = $this->getParameter('ipc_security.credentials_expired.flash_bag');
        $targetRoute     = $this->getParameter('ipc_security.credentials_expired.target_route');

        $model = new ChangePassword();
        $form  = $this->createForm($formClass, $model, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /* @var $user AdvancedUserTrait */
            $user
                ->setCredentialsExpired(false)
                ->setPassword($encoderFactory->getEncoder($user)->encodePassword($model->getNew(), $user->getSalt()));

            $manager = $managerRegistry->getManagerForClass(\get_class($user));
            $manager->persist($user);
            $manager->flush();

            if ($flashBagOptions && $flashBagOptions['type']['success']) {
                $type = $flashBagOptions['type']['success'];
                $this->addFlash($type, $this->translator->trans('ipc_security.credentials_expired.update_success'));
            }

            return $this->redirectToRoute($targetRoute);
        }

        if ($flashBagOptions && $flashBagOptions['type']['error']) {
            $type = $flashBagOptions['type']['error'];
            foreach ($form->getErrors() as $error) {
                $this->addFlash($type, $this->translator->trans($error->getMessage()));
            }
        }

        return $this->render($viewTemplate, [
            'change_password' => $form->createView(),
            'options'         => $options,
        ]);
    }
}
