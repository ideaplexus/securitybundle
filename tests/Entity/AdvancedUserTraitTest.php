<?php

namespace IPC\Tests\SecurityBundle\Entity;

use IPC\SecurityBundle\Entity\AdvancedUserTrait;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \IPC\SecurityBundle\Entity\AdvancedUserTrait
 */
class AdvancedUserTraitTest extends TestCase
{
    /**
     * @var AdvancedUserTrait
     */
    protected $trait;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->trait = $this->getMockForTrait(AdvancedUserTrait::class);
    }

    /**
     * @return void
     *
     * @covers ::isExpired()
     * @covers ::setExpired()
     * @covers ::isLocked()
     * @covers ::setLocked()
     * @covers ::isEnabled()
     * @covers ::setEnabled()
     * @covers ::isCredentialsExpired()
     * @covers ::setCredentialsExpired
     */
    public function testGetterSetter(): void
    {
        $this->assertFalse($this->trait->isExpired());
        $this->assertEquals($this->trait, $this->trait->setExpired(true));
        $this->assertTrue($this->trait->isExpired());

        $this->assertFalse($this->trait->isLocked());
        $this->assertEquals($this->trait, $this->trait->setLocked(true));
        $this->assertTrue($this->trait->isLocked());

        $this->assertFalse($this->trait->isEnabled());
        $this->assertEquals($this->trait, $this->trait->setEnabled(true));
        $this->assertTrue($this->trait->isEnabled());

        $this->assertFalse($this->trait->isCredentialsExpired());
        $this->assertEquals($this->trait, $this->trait->setCredentialsExpired(true));
        $this->assertTrue($this->trait->isCredentialsExpired());
    }

    /**
     * @return void
     *
     * @covers ::serialize()
     * @covers ::unserialize()
     */
    public function testSerializeUnserialize(): void
    {
        $this->trait->setUsername('user');
        $this->trait->setPassword('pass');
        $this->trait->setSalt('salt');
        $this->trait->setLocked(true);
        $this->trait->setExpired(true);
        $this->trait->setCredentialsExpired(true);
        $this->trait->setEnabled(true);
        $this->trait->addRole('role');


        $serialized = $this->trait->serialize();

        $this->trait->setUsername('name');
        $this->trait->setPassword('word');
        $this->trait->setSalt('hash');
        $this->trait->setExpired(false);
        $this->trait->setLocked(false);
        $this->trait->setEnabled(false);
        $this->trait->setCredentialsExpired(false);
        $this->trait->removeRole('role');

        $this->trait->unserialize($serialized);

        $this->assertEquals('user', $this->trait->getUsername());
        $this->assertEquals('pass', $this->trait->getPassword());
        $this->assertEquals('salt', $this->trait->getSalt());
        $this->assertTrue($this->trait->hasRole('role'));
        $this->assertTrue($this->trait->isExpired());
        $this->assertTrue($this->trait->isLocked());
        $this->assertTrue($this->trait->isEnabled());
        $this->assertTrue($this->trait->isCredentialsExpired());
    }
}
