<?php

namespace IPC\Tests\SecurityBundle\Controller;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ObjectManager;
use IPC\SecurityBundle\Controller\SecurityController;
use IPC\SecurityBundle\Entity\AdvancedUser;
use IPC\SecurityBundle\Form\Model\ChangePassword;
use IPC\SecurityBundle\Form\Type\ChangePasswordType;
use IPC\SecurityBundle\Form\Type\LoginType;
use IPC\SecurityBundle\User\DoctrineUserProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @coversDefaultClass \IPC\SecurityBundle\Controller\SecurityController
 */
class SecurityControllerTest extends TestCase
{
    /**
     * @return void
     *
     * @covers ::__construct()
     */
    public function test__construct(): void
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $instance   = new SecurityController($translator);
        $reflection = new \ReflectionProperty($instance, 'translator');
        $reflection->setAccessible(true);

        $this->assertEquals($translator, $reflection->getValue($instance));
    }

    /**
     * @return void
     *
     * @covers ::login()
     */
    public function testLoginRedirectCredentialsExpired(): void
    {
        $loginForm = LoginType::class;
        $loginView = '@IPCSecurity/login.html.twig';
        $username  = 'username';

        $translator = $this->createMock(TranslatorInterface::class);
        $container  = $this->createMock(Container::class);
        $authUtils  = $this->createMock(AuthenticationUtils::class);
        $error      = $this->createMock(CredentialsExpiredException::class);
        $response   = $this->createMock(RedirectResponse::class);

        $container
            ->expects($this->exactly(5))
            ->method('getParameter')
            ->willReturnCallback(function(string $name) use ($loginForm, $loginView)  {
                switch ($name) {
                    case 'ipc_security.login.form':
                        return $loginForm;
                    case 'ipc_security.login.view':
                        return $loginView;
                    case 'ipc_security.login.flash_bag':
                        return ['type' => ['error' => 'error']];
                    case 'ipc_security.credentials_expired.enabled':
                        return true;
                    case 'ipc_security.credentials_expired.route':
                        return 'credentials_expired';
                }
            });

        $authUtils
            ->expects($this->once())
            ->method('getLastAuthenticationError')
            ->willReturn($error);

        $authUtils
            ->expects($this->once())
            ->method('getLastUsername')
            ->willReturn($username);

        $instance = $this->getMockBuilder(SecurityController::class)
            ->setConstructorArgs([$translator])
            ->setMethods(['redirectToRoute'])
            ->getMock();

        $instance->setContainer($container);

        $instance
            ->expects($this->once())
            ->method('redirectToRoute')
            ->willReturn($response);


        $this->assertEquals($response, $instance->login($authUtils));
    }

    /**
     * @return void
     *
     * @covers ::login()
     */
    public function testLoginCredentialsExpiredAndFlashBag(): void
    {
        $loginForm    = LoginType::class;
        $loginView    = '@IPCSecurity/login.html.twig';
        $username     = 'username';
        $errorMessage = 'error message';
        $formView     = 'form view';

        $translator = $this->createMock(TranslatorInterface::class);
        $container  = $this->createMock(Container::class);
        $authUtils  = $this->createMock(AuthenticationUtils::class);
        $response   = $this->createMock(RedirectResponse::class);
        $form       = $this->createMock(FormInterface::class);
        $error      = new CredentialsExpiredException($errorMessage);

        $container
            ->expects($this->exactly(5))
            ->method('getParameter')
            ->willReturnCallback(function(string $name) use ($loginForm, $loginView)  {
                switch ($name) {
                    case 'ipc_security.login.form':
                        return $loginForm;
                    case 'ipc_security.login.view':
                        return $loginView;
                    case 'ipc_security.login.flash_bag':
                        return ['type' => ['error' => 'error']];
                    case 'ipc_security.credentials_expired.enabled':
                        return false;
                    case 'ipc_security.credentials_expired.route':
                        return 'credentials_expired';
                }
            });

        $form
            ->expects($this->once())
            ->method('createView')
            ->willReturn($formView);

        $translator
            ->expects($this->once())
            ->method('trans')
            ->with($errorMessage)
            ->willReturn($errorMessage);

        $authUtils
            ->expects($this->once())
            ->method('getLastAuthenticationError')
            ->willReturn($error);

        $authUtils
            ->expects($this->once())
            ->method('getLastUsername')
            ->willReturn($username);

        $instance = $this->getMockBuilder(SecurityController::class)
            ->setConstructorArgs([$translator])
            ->setMethods(['addFlash', 'createForm', 'render'])
            ->getMock();

        $instance->setContainer($container);

        $instance
            ->expects($this->once())
            ->method('addFlash')
            ->with('error', $errorMessage)
            ->willReturn($response);

        $instance
            ->expects($this->once())
            ->method('createForm')
            ->with($loginForm)
            ->willReturn($form);

        $instance
            ->expects($this->once())
            ->method('render')
            ->with($loginView, [
                'last_username' => $username,
                'error'         => $error,
                'login'         => $formView,
            ])
            ->willReturn($response);


        $this->assertEquals($response, $instance->login($authUtils));
    }

    /**
     * @return void
     *
     * @covers ::credentialsExpired()
     */
    public function testCredentialsExpiredNotEnabled(): void
    {
        $translator      = $this->createMock(TranslatorInterface::class);
        $request         = $this->createMock(Request::class);
        $authUtils       = $this->createMock(AuthenticationUtils::class);
        $tokenStorage    = $this->createMock(TokenStorageInterface::class);
        $userProvider    = $this->createMock(DoctrineUserProvider::class);
        $encoderFactory  = $this->createMock(EncoderFactoryInterface::class);
        $managerRegistry = $this->createMock(ManagerRegistry::class);
        $exception       = $this->createMock(AccessDeniedException::class);

        $translator
            ->expects($this->exactly(2))
            ->method('trans')
            ->willReturnCallback(function ($input) {
                return $input;
            });

        $instance = $this->getMockBuilder(SecurityController::class)
            ->setConstructorArgs([$translator])
            ->setMethods(['getParameter', 'createAccessDeniedException'])
            ->getMock();

        $instance
            ->expects($this->once())
            ->method('getParameter')
            ->with('ipc_security.credentials_expired.enabled')
            ->willReturn(false);

        $instance
            ->expects($this->once())
            ->method('createAccessDeniedException')
            ->willReturnCallback(function (string $message, \Exception $previous) use ($exception) {
                $this->assertEquals('ipc_security.credentials_expired.access_denied', $message);
                $this->assertInstanceOf(\RuntimeException::class, $previous);
                $this->assertEquals('ipc_security.credentials_expired.not_enabled', $previous->getMessage());
                return $exception;
            });

        $this->expectException(AccessDeniedException::class);

        $instance->credentialsExpired(
            $request,
            $authUtils,
            $tokenStorage,
            $userProvider,
            $encoderFactory,
            $managerRegistry
        );
    }


    /**
     * @return void
     *
     * @covers ::credentialsExpired()
     */
    public function testCredentialsExpiredUserNotSupported(): void
    {
        $username = 'username';

        $user            = $this->createMock(AdvancedUser::class);
        $translator      = $this->createMock(TranslatorInterface::class);
        $request         = $this->createMock(Request::class);
        $authUtils       = $this->createMock(AuthenticationUtils::class);
        $tokenStorage    = $this->createMock(TokenStorageInterface::class);
        $userProvider    = $this->createMock(DoctrineUserProvider::class);
        $encoderFactory  = $this->createMock(EncoderFactoryInterface::class);
        $managerRegistry = $this->createMock(ManagerRegistry::class);
        $exception       = $this->createMock(AccessDeniedException::class);

        $authUtils
            ->expects($this->once())
            ->method('getLastUsername')
            ->willReturn($username);

        $userProvider
            ->expects($this->once())
            ->method('loadUserByUsername')
            ->with($username)
            ->willReturn($user);

        $user
            ->expects($this->once())
            ->method('isCredentialsExpired')
            ->willReturn(false);

        $translator
            ->expects($this->exactly(2))
            ->method('trans')
            ->willReturnCallback(function ($input) {
                return $input;
            });

        $instance = $this->getMockBuilder(SecurityController::class)
            ->setConstructorArgs([$translator])
            ->setMethods(['getParameter', 'createAccessDeniedException'])
            ->getMock();

        $instance
            ->expects($this->once())
            ->method('getParameter')
            ->with('ipc_security.credentials_expired.enabled')
            ->willReturn(true);

        $instance
            ->expects($this->once())
            ->method('createAccessDeniedException')
            ->willReturnCallback(function (string $message, \Exception $previous) use ($exception) {
                $this->assertEquals('ipc_security.credentials_expired.access_denied', $message);
                $this->assertInstanceOf(\RuntimeException::class, $previous);
                $this->assertEquals('ipc_security.credentials_expired.user_not_supported', $previous->getMessage());
                return $exception;
            });


        $this->expectException(AccessDeniedException::class);

        $instance->credentialsExpired(
            $request,
            $authUtils,
            $tokenStorage,
            $userProvider,
            $encoderFactory,
            $managerRegistry
        );
    }

    /**
     * @return void
     *
     * @covers ::credentialsExpired()
     */
    public function testCredentialsExpiredFormError(): void
    {
        $username                      = 'username';
        $credentialsExpiredForm        = ChangePasswordType::class;
        $credentialsExpiredFormOptions = [];
        $credentialsExpiredView        = '@IPCSecurity/change_password.html.twig';
        $errorMessage                  = 'error message';
        $formView                      = 'form view';

        $user            = $this->createMock(AdvancedUser::class);
        $translator      = $this->createMock(TranslatorInterface::class);
        $request         = $this->createMock(Request::class);
        $authUtils       = $this->createMock(AuthenticationUtils::class);
        $tokenStorage    = $this->createMock(TokenStorageInterface::class);
        $userProvider    = $this->createMock(DoctrineUserProvider::class);
        $encoderFactory  = $this->createMock(EncoderFactoryInterface::class);
        $managerRegistry = $this->createMock(ManagerRegistry::class);
        $form            = $this->createMock(FormInterface::class);
        $error           = $this->createMock(FormError::class);
        $response        = $this->createMock(Response::class);

        $authUtils
            ->expects($this->once())
            ->method('getLastUsername')
            ->willReturn($username);

        $userProvider
            ->expects($this->once())
            ->method('loadUserByUsername')
            ->with($username)
            ->willReturn($user);

        $user
            ->expects($this->once())
            ->method('isCredentialsExpired')
            ->willReturn(true);

        $tokenStorage
            ->expects($this->once())
            ->method('setToken')
            ->willReturnCallback(function(TokenInterface $token) use ($user) {
                $this->assertInstanceOf(UsernamePasswordToken::class, $token);
                $this->assertEquals($user, $token->getUser());
                $this->assertEquals('', $token->getCredentials());
                $this->assertEquals('_', $token->getProviderKey());
            });

        $form
            ->expects($this->once())
            ->method('isSubmitted')
            ->willReturn(true);

        $form
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(false);

        $form
            ->expects($this->once())
            ->method('getErrors')
            ->willReturn([$error]);

        $form
            ->expects($this->once())
            ->method('createView')
            ->willReturn($formView);

        $error
            ->expects($this->once())
            ->method('getMessage')
            ->willReturn($errorMessage);

        $translator
            ->expects($this->once())
            ->method('trans')
            ->with($errorMessage)
            ->willReturn($errorMessage);

        $instance = $this->getMockBuilder(SecurityController::class)
            ->setConstructorArgs([$translator])
            ->setMethods(['getParameter', 'createForm', 'addFlash', 'render'])
            ->getMock();

        $instance
            ->expects($this->exactly(6))
            ->method('getParameter')
            ->willReturnCallback(function(string $name)
            use ($credentialsExpiredForm, $credentialsExpiredView, $credentialsExpiredFormOptions)
            {
                switch ($name) {
                    case 'ipc_security.credentials_expired.enabled':
                        return true;
                    case 'ipc_security.credentials_expired.form':
                        return $credentialsExpiredForm;
                    case 'ipc_security.credentials_expired.form_options':
                        return $credentialsExpiredFormOptions;
                    case 'ipc_security.credentials_expired.view':
                        return $credentialsExpiredView;
                    case 'ipc_security.credentials_expired.flash_bag':
                        return ['type' => ['error' => 'error', 'success' => 'success']];
                    case 'ipc_security.credentials_expired.target_route':
                        return 'login';
                }
            });

        $instance
            ->expects($this->once())
            ->method('createForm')
            ->willReturnCallback(function($formClass, $model, $options)
                use ($credentialsExpiredForm, $credentialsExpiredFormOptions, $form)
            {
                $this->assertEquals($credentialsExpiredForm, $formClass);
                $this->assertInstanceOf(ChangePassword::class, $model);
                $this->assertEquals($credentialsExpiredFormOptions, $options);

                return $form;
            });

        $instance
            ->expects($this->once())
            ->method('addFlash')
            ->with('error', $errorMessage);

        $instance
            ->expects($this->once())
            ->method('render')
            ->with($credentialsExpiredView, [
                'change_password' => $formView,
                'options'         => $credentialsExpiredFormOptions,
            ])
            ->willReturn($response);

        $this->assertEquals($response, $instance->credentialsExpired(
            $request,
            $authUtils,
            $tokenStorage,
            $userProvider,
            $encoderFactory,
            $managerRegistry
        ));
    }

    /**
     * @return void
     *
     * @covers ::credentialsExpired()
     */
    public function testCredentialsExpiredFormValid(): void
    {
        $username                      = 'username';
        $credentialsExpiredForm        = ChangePasswordType::class;
        $credentialsExpiredFormOptions = [];
        $credentialsExpiredView        = '@IPCSecurity/change_password.html.twig';
        $successMessage                = 'ipc_security.credentials_expired.update_success';
        $password                      = 'password';
        $targetRoute                   = 'login';

        $user            = $this->createMock(AdvancedUser::class);
        $translator      = $this->createMock(TranslatorInterface::class);
        $request         = $this->createMock(Request::class);
        $authUtils       = $this->createMock(AuthenticationUtils::class);
        $tokenStorage    = $this->createMock(TokenStorageInterface::class);
        $userProvider    = $this->createMock(DoctrineUserProvider::class);
        $encoderFactory  = $this->createMock(EncoderFactoryInterface::class);
        $managerRegistry = $this->createMock(ManagerRegistry::class);
        $form            = $this->createMock(FormInterface::class);
        $encoder         = $this->createMock(PasswordEncoderInterface::class);
        $manager         = $this->createMock(ObjectManager::class);
        $response        = $this->createMock(RedirectResponse::class);

        $authUtils
            ->expects($this->once())
            ->method('getLastUsername')
            ->willReturn($username);

        $userProvider
            ->expects($this->once())
            ->method('loadUserByUsername')
            ->with($username)
            ->willReturn($user);

        $user
            ->expects($this->once())
            ->method('isCredentialsExpired')
            ->willReturn(true);

        $user
            ->expects($this->once())
            ->method('setCredentialsExpired')
            ->with(false)
            ->willReturnSelf();

        $user
            ->expects($this->once())
            ->method('setPassword')
            ->with($password)
            ->willReturnSelf();

        $tokenStorage
            ->expects($this->once())
            ->method('setToken')
            ->willReturnCallback(function(TokenInterface $token) use ($user) {
                $this->assertInstanceOf(UsernamePasswordToken::class, $token);
                $this->assertEquals($user, $token->getUser());
                $this->assertEquals('', $token->getCredentials());
                $this->assertEquals('_', $token->getProviderKey());
            });

        $form
            ->expects($this->once())
            ->method('isSubmitted')
            ->willReturn(true);

        $form
            ->expects($this->once())
            ->method('isValid')
            ->willReturn(true);

        $translator
            ->expects($this->once())
            ->method('trans')
            ->with($successMessage)
            ->willReturn($successMessage);

        $encoderFactory
            ->expects($this->once())
            ->method('getEncoder')
            ->with($user)
            ->willReturn($encoder);

        $encoder
            ->expects($this->once())
            ->method('encodePassword')
            ->with($password, null)
            ->willReturn($password);

        $managerRegistry
            ->expects($this->once())
            ->method('getManagerForClass')
            ->willReturn($manager);

        $manager
            ->expects($this->once())
            ->method('persist')
            ->with($user);

        $manager
            ->expects($this->once())
            ->method('flush');

        $instance = $this->getMockBuilder(SecurityController::class)
            ->setConstructorArgs([$translator])
            ->setMethods(['getParameter', 'createForm', 'addFlash', 'redirectToRoute'])
            ->getMock();

        $instance
            ->expects($this->exactly(6))
            ->method('getParameter')
            ->willReturnCallback(function(string $name)
            use ($credentialsExpiredForm, $credentialsExpiredView, $credentialsExpiredFormOptions, $targetRoute)
            {
                switch ($name) {
                    case 'ipc_security.credentials_expired.enabled':
                        return true;
                    case 'ipc_security.credentials_expired.form':
                        return $credentialsExpiredForm;
                    case 'ipc_security.credentials_expired.form_options':
                        return $credentialsExpiredFormOptions;
                    case 'ipc_security.credentials_expired.view':
                        return $credentialsExpiredView;
                    case 'ipc_security.credentials_expired.flash_bag':
                        return ['type' => ['error' => 'error', 'success' => 'success']];
                    case 'ipc_security.credentials_expired.target_route':
                        return $targetRoute;
                }
            });

        $instance
            ->expects($this->once())
            ->method('createForm')
            ->willReturnCallback(function($formClass, $model, $options)
            use ($credentialsExpiredForm, $credentialsExpiredFormOptions, $form, $password)
            {
                $this->assertEquals($credentialsExpiredForm, $formClass);
                $this->assertInstanceOf(ChangePassword::class, $model);
                $this->assertEquals($credentialsExpiredFormOptions, $options);

                $model->setNew($password);
                return $form;
            });

        $instance
            ->expects($this->once())
            ->method('addFlash')
            ->with('success', $successMessage);

        $instance
            ->expects($this->once())
            ->method('redirectToRoute')
            ->with($targetRoute)
            ->willReturn($response);

        $instance->setContainer($container);

        $this->assertEquals($response, $instance->credentialsExpired(
            $request,
            $authUtils,
            $tokenStorage,
            $userProvider,
            $encoderFactory,
            $managerRegistry
        ));
    }
}
