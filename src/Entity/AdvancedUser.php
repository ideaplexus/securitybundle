<?php

namespace IPC\SecurityBundle\Entity;

class AdvancedUser implements UserInterface
{
    use AdvancedUserTrait;
}
