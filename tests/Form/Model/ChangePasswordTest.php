<?php

namespace IPC\Tests\SecurityBundle\Form\Model;

use IPC\SecurityBundle\Form\Model\ChangePassword;
use IPC\ValidatorBundle\Validator\Constraints\Chain;
use IPC\ValidatorBundle\Validator\Constraints\EqualProperties;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Mapping\Loader\YamlFileLoader;

/**
 * @coversDefaultClass \IPC\SecurityBundle\Form\Model\ChangePassword
 */
class ChangePasswordTest extends KernelTestCase
{
    /**
     * @var ChangePassword
     */
    protected $changePassword;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->changePassword = new ChangePassword();
    }

    /**
     * @return void
     *
     * @covers ::<public>
     */
    public function testSetGet(): void
    {
        $current = 'current';
        $this->assertEquals($this->changePassword, $this->changePassword->setCurrent($current));
        $this->assertEquals($current, $this->changePassword->getCurrent());
        $this->assertEquals($this->changePassword, $this->changePassword->setCurrent(null));
        $this->assertNull($this->changePassword->getCurrent());

        $new = 'new';
        $this->assertEquals($this->changePassword, $this->changePassword->setNew($new));
        $this->assertEquals($new, $this->changePassword->getNew());
        $this->assertEquals($this->changePassword, $this->changePassword->setNew(null));
        $this->assertNull($this->changePassword->getNew());

        $repeated = 'repeated';
        $this->assertEquals($this->changePassword, $this->changePassword->setRepeated($repeated));
        $this->assertEquals($repeated, $this->changePassword->getRepeated());
        $this->assertEquals($this->changePassword, $this->changePassword->setRepeated(null));
        $this->assertNull($this->changePassword->getRepeated());
    }

    /**
     * @return void
     *
     * @coversNothing
     */
    public function testValidation(): void
    {
        $metadataFactory = new LazyLoadingMetadataFactory(new YamlFileLoader(
            __DIR__ . '/../../../src/Resources/config/validation/ChangePasswordModel.yaml'
        ));

        $metadata = $metadataFactory->getMetadataFor(ChangePassword::class);

        $this->assertTrue($metadata->hasPropertyMetadata('current'));

        $current = $metadata->getPropertyMetadata('current')[0];

        $constraints = $current->getConstraints();
        $this->assertCount(1, $constraints);

        /* @var $chain Chain */
        $chain = $constraints[0];
        $this->assertInstanceOf(Chain::class, $chain);
        $this->assertCount(2, $chain->constraints);
        $this->assertTrue($chain->stopOnError);
        $this->assertEquals(['require_current'], $chain->groups);

        /* @var $notBlank NotBlank */
        $notBlank = $chain->constraints[0];
        $this->assertInstanceOf(NotBlank::class, $notBlank);
        $this->assertEquals('form.validation.change_password.current.not_blank', $notBlank->message);

        /* @var $userPassword UserPassword */
        $userPassword = $chain->constraints[1];
        $this->assertInstanceOf(UserPassword::class, $userPassword);
        $this->assertEquals('form.validation.change_password.current.user_password', $userPassword->message);

        $this->assertTrue($metadata->hasPropertyMetadata('new'));

        $new = $metadata->getPropertyMetadata('new')[0];

        $constraints = $new->getConstraints();
        $this->assertCount(1, $constraints);

        /* @var $notBlank NotBlank */
        $notBlank = $constraints[0];
        $this->assertInstanceOf(NotBlank::class, $notBlank);
        $this->assertEquals('form.validation.change_password.new.not_blank', $notBlank->message);
        $this->assertEquals(['Default', 'ChangePassword'], $notBlank->groups);

        $propertyConstraints = $metadata->getConstraints();

        $this->assertCount(1, $propertyConstraints);

        /* @var $equalProperties EqualProperties */
        $equalProperties = $propertyConstraints[0];

        $this->assertInstanceOf(EqualProperties::class, $equalProperties);
        $this->assertEquals('form.validation.change_password.constraints.equal_properties', $equalProperties->message);
        $this->assertEquals(['new', 'current'], $equalProperties->properties);
        $this->assertEquals(['require_current'], $equalProperties->groups);
        $this->assertTrue($equalProperties->invert);
        $this->assertFalse($equalProperties->skipNull);
    }
}
