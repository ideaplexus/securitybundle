<?php

namespace IPC\Tests\SecurityBundle\Entity;

use IPC\SecurityBundle\Entity\CoreUserTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @coversDefaultClass \IPC\SecurityBundle\Entity\CoreUserTrait
 */
class CoreUserTraitTest extends TestCase
{
    /**
     * @var CoreUserTrait
     */
    protected $trait;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->trait = $this->getMockForTrait(CoreUserTrait::class);
    }

    /**
     * @covers ::setUsername()
     * @covers ::getUserName()
     * @covers ::setPassword()
     * @covers ::getPassword()
     * @covers ::setPlainPassword()
     * @covers ::getPlainPassword()
     * @covers ::setSalt()
     * @covers ::getSalt()
     * @covers ::getRoles()
     * @covers ::addRole()
     * @covers ::removeRole()
     * @covers ::hasRole()
     */
    public function testGetterSetter(): void
    {
        $this->assertEquals($this->trait, $this->trait->setUsername('user'));
        $this->assertEquals('user', $this->trait->getUsername());

        $this->assertEquals($this->trait, $this->trait->setPassword('pass'));
        $this->assertEquals('pass', $this->trait->getPassword());

        $this->assertEquals($this->trait, $this->trait->setPlainPassword(null));
        $this->assertEquals(null, $this->trait->getPlainPassword());
        $this->assertEquals($this->trait, $this->trait->setPlainPassword('plain'));
        $this->assertEquals('plain', $this->trait->getPlainPassword());

        $this->assertEquals($this->trait, $this->trait->setSalt('salt'));
        $this->assertEquals('salt', $this->trait->getSalt());

        $this->assertEquals([], $this->trait->getRoles());
        $this->assertEquals($this->trait, $this->trait->addRole('role'));
        $this->assertTrue($this->trait->hasRole('role'));
        $this->assertEquals(['role'], $this->trait->getRoles());
        $this->assertEquals($this->trait, $this->trait->removeRole('role'));
        $this->assertFalse($this->trait->hasRole('role'));
        $this->assertEquals([], $this->trait->getRoles());
    }

    /**
     * @covers ::eraseCredentials()
     */
    public function testEraseCredentials(): void
    {
        $this->assertEquals($this->trait, $this->trait->setPlainPassword('plain'));
        $this->assertEquals($this->trait, $this->trait->eraseCredentials());
        $this->assertEquals(null, $this->trait->getPlainPassword());
    }

    /**
     * @return void
     *
     * @covers ::serialize()
     * @covers ::unserialize()
     */
    public function testSerializeUnserialize(): void
    {
        $this->trait->setUsername('user');
        $this->trait->setPassword('pass');
        $this->trait->setSalt('salt');
        $this->trait->addRole('role');

        $serialized = $this->trait->serialize();

        $this->trait->setUsername('name');
        $this->trait->setPassword('word');
        $this->trait->setSalt('hash');
        $this->trait->removeRole('role');

        $this->trait->unserialize($serialized);

        $this->assertEquals('user', $this->trait->getUsername());
        $this->assertEquals('pass', $this->trait->getPassword());
        $this->assertEquals('salt', $this->trait->getSalt());
        $this->assertTrue($this->trait->hasRole('role'));
    }

    /**
     * @return void
     *
     * @covers ::__toString()
     */
    public function test__toString(): void
    {
        $this->assertEquals($this->trait, $this->trait->setUsername('user'));
        $this->assertEquals('user', $this->trait->__toString());
    }

    /**
     * @return void
     *
     * @covers ::isEqualTo()
     */
    public function testIsEqualToEmptyUser(): void
    {
        $userInterface = $this->getMockForAbstractClass(UserInterface::class);

        $this->assertFalse($this->trait->isEqualTo($userInterface));
    }

    /**
     * @return void
     *
     * @covers ::isEqualTo()
     */
    public function testIsEqualTo(): void
    {
        $userInterface = $this->getMockForAbstractClass(UserInterface::class);

        $userInterface
            ->expects($this->once())
            ->method('getUsername')
            ->willReturn('user');

        $userInterface
            ->expects($this->once())
            ->method('getPassword')
            ->willReturn('pass');

        $userInterface
            ->expects($this->once())
            ->method('getSalt')
            ->willReturn('salt');


        $this->trait->setUsername('user');
        $this->trait->setPassword('pass');
        $this->trait->setSalt('salt');

        $this->assertTrue($this->trait->isEqualTo($userInterface));
    }
}
