<?php

namespace IPC\SecurityBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IPCSecurityBundle extends Bundle
{
    public const BUNDLE_PREFIX = 'ipc_security';
}
