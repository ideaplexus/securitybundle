<?php

namespace IPC\SecurityBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

trait CoreUserTrait
{
    /**
     * Username
     *
     * @var string
     */
    protected $username;

    /**
     * Plain password
     *
     * @var string|null
     */
    protected $plainPassword;

    /**
     * Password
     *
     * @var string|null
     */
    protected $password;

    /**
     * Salt
     *
     * @var string|null
     */
    protected $salt;

    /**
     * Roles
     *
     * @var string[]
     */
    protected $roles = [];

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function addRole(string $role): self
    {
        if (!$this->hasRole($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        return \in_array($role, $this->roles, true);
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function removeRole(string $role): self
    {
        $index = array_search($role, $this->roles, true);
        if ($index !== false) {
            unset($this->roles[$index]);
        }

        return $this;
    }

    /**
     * @param string|null $plainPassword
     *
     * @return $this
     */
    public function setPlainPassword(?string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $password
     *
     * @return $this
     */
    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $salt
     *
     * @return $this
     */
    public function setSalt(?string $salt): self
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @return $this
     */
    public function eraseCredentials(): self
    {
        $this->setPlainPassword(null);

        return $this;
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize([
            $this->username,
            $this->password,
            $this->salt,
            $this->roles,
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized): void
    {
        [
            $this->username,
            $this->password,
            $this->salt,
            $this->roles,
        ] = unserialize($serialized, ['allowed_classes' => [self::class]]);
    }

    /**
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isEqualTo(UserInterface $user): bool
    {
        try {
            $userKey = implode('', [
                $user->getUsername(),
                $user->getPassword(),
                $user->getSalt(),
            ]);
            $selfKey = implode('', [
                $this->getUsername(),
                $this->getPassword(),
                $this->getSalt(),
            ]);
        } catch (\TypeError $error) {
            return false;
        }

        return $userKey === $selfKey;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->username;
    }
}
