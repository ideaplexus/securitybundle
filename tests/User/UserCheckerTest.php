<?php

namespace IPC\Tests\SecurityBundle\User;

use IPC\SecurityBundle\Entity\AdvancedUser;
use IPC\SecurityBundle\Entity\CoreUser;
use IPC\SecurityBundle\User\UserChecker;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;

/**
 * @coversDefaultClass \IPC\SecurityBundle\User\UserChecker
 */
class UserCheckerTest extends TestCase
{
    /**
     * @var UserChecker
     */
    protected $userChecker;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->userChecker = new UserChecker();
    }

    /**
     * @return void
     *
     * @throws LockedException
     *
     * @covers ::checkPreAuth()
     */
    public function testCheckPreAuthIsLocked(): void
    {
        $user = $this->createMock(AdvancedUser::class);

        $user
            ->expects($this->once())
            ->method('isLocked')
            ->willReturn(true);

        $this->expectException(LockedException::class);

        $this->userChecker->checkPreAuth($user);
    }

    /**
     * @return void
     *
     * @throws DisabledException
     *
     * @covers ::checkPreAuth()
     */
    public function testCheckPreAuthIsEnabled(): void
    {
        $user = $this->createMock(AdvancedUser::class);

        $user
            ->expects($this->once())
            ->method('isEnabled')
            ->willReturn(false);

        $this->expectException(DisabledException::class);

        $this->userChecker->checkPreAuth($user);
    }

    /**
     * @return void
     *
     * @throws AccountExpiredException
     *
     * @covers ::checkPreAuth()
     */
    public function testCheckPreAuthIsExpired(): void
    {
        $user = $this->createMock(AdvancedUser::class);

        $user
            ->expects($this->once())
            ->method('isEnabled')
            ->willReturn(true);

        $user
            ->expects($this->once())
            ->method('isExpired')
            ->willReturn(true);

        $this->expectException(AccountExpiredException::class);

        $this->userChecker->checkPreAuth($user);
    }

    /**
     * @return void
     *
     * @covers ::checkPreAuth()
     */
    public function testCheckPreAuthNonAdvancedUser(): void
    {
        $user = $this->createMock(CoreUser::class);

        $this->userChecker->checkPreAuth($user);

        $this->assertTrue(true);
    }


    /**
     * @return void
     *
     * @throws CredentialsExpiredException
     *
     * @covers ::checkPostAuth()
     */
    public function testCheckPostAuth(): void
    {
        $user = $this->createMock(AdvancedUser::class);

        $user
            ->expects($this->once())
            ->method('isCredentialsExpired')
            ->willReturn(true);

        $this->expectException(CredentialsExpiredException::class);

        $this->userChecker->checkPostAuth($user);
    }

    /**
     * @return void
     *
     * @covers ::checkPostAuth()
     */
    public function testCheckPostAuthNonAdvancedUser(): void
    {
        $user = $this->createMock(CoreUser::class);

        $this->userChecker->checkPostAuth($user);

        $this->assertTrue(true);
    }
}
